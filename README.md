`search_notes.py` --- a simple python script to look for a pattern in .mu2 files
---

Description
---
This script prints the filename of the files containing the pattern in 
`patternfile`.

`patternfile` is a list of notes in latin notation (do re mi fa sol la si), one
note per line, no space, lowercase, no accents

Usage
---
`search_notes.py -f patternfile -d mu2files_dir`

Use case
---
It has been written to find a score in https://github.com/MTG/SymbTr, knowing the notes of a part of the piece
