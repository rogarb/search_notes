#!/usr/bin/env python3

import argparse
import pathlib
import os.path
import os
import sys
import re
import glob

class Comparer:
    def __init__(self, pattern, mu2file):
        self.notes = []
        self.name = mu2file
        self.pattern = pattern
        f = open(mu2file, 'r')
        while line := f.readline():
            if line.startswith("9\t"):
                note = re.sub(r"9\t([a-zA-Z]*)[0-9].*\n", r"\1", line)
                if not (note == "" or note == line):
                    self.notes.append(note.lower())

    def print(self):
        print("pattern: ", self.pattern)
        print("name: ", self.name)
        print("notes: ", self.notes) 

    def matches(self):
        first = self.pattern[0]
        i = 0
        n = len(self.pattern)
        while True:
            try:
                i = self.notes.index(first, i)
            except ValueError:
                return False
            if self.pattern == self.notes[i:i+n]:
                return True
            i += 1
    
    def compare(self):
        if self.matches():
            print(self.name, " matches")

# Compulsory arguments: 
#   -f <note_file>
#   -d <mu2 file directory>

parser = argparse.ArgumentParser(description='Search for a musical pattern in .mu2 files')
parser.add_argument('-f', nargs=1, dest='pattern_file', required=True, type=argparse.FileType('r'))
parser.add_argument('-d', nargs=1, dest='mu2dir', required=True, type=pathlib.Path)

args = parser.parse_args()

if not args.mu2dir[0].is_dir():
    print(args.mu2dir[0], " doesn't exist")
    parser.print_help()
    sys.exit(1)

pattern = args.pattern_file[0].read().strip('\n').split('\n')

for f in args.mu2dir[0].glob("*.mu2"):
    comp = Comparer(pattern, f)
    comp.compare()

